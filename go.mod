module code.rocketnine.space/tslocum/cview

go 1.12

require (
	code.rocketnine.space/tslocum/cbind v0.1.5
	github.com/gdamore/tcell/v2 v2.2.1-0.20210305060500-f4d402906fa3
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/mattn/go-runewidth v0.0.12
	github.com/rivo/uniseg v0.2.0
	golang.org/x/sys v0.0.0-20210331175145-43e1dd70ce54 // indirect
	golang.org/x/term v0.0.0-20210317153231-de623e64d2a6 // indirect
	golang.org/x/text v0.3.6 // indirect
)
